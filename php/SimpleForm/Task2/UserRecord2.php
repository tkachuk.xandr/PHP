<?php
include_once "../Task1/UserRecord.php";
class UserRecord2 extends UserRecord{

    protected $fileName;
    protected $uploadPath;

    function __construct() {
        $this->readUserData();
        $this->createUploadPath();
        $this->setFileName();
        $this->createOutText();
        $this->saveUserData((string)$this->uploadPath."/"."zayavka.txt");
    }

    function readUserData() {
        $this->name = $_POST['name'];
    }

    function createOutText()
    {
        $this->outText = $this->name.'  '.$this->getTime()."\r\n";
    }

    function setFileName() {
        //$tmpFilePath = "F:/Source/php/SimpleForm/Task2/";
        $this->fileName = $this->uploadPath."/".$_FILES['userfile']['name'];
    }

    function createUploadPath () {
        $this->uploadPath = strftime("%d_%m_%Y__%H_%M_%S");
    }

    function saveUserData($filePath) {
        mkdir($this->uploadPath);
        parent::saveUserData($filePath);

        copy((string)$_FILES['userfile']['tmp_name'], $this->fileName);
    }

}