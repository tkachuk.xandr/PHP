<?php

class UserRecord
{

    protected $name;
    protected $password;

    protected $outText;

    function __construct(){
        $this->readUserData();
        $this->createOutText();
        $this->saveUserData("zayavka.txt");
    }

    function readUserData(){
        $this->name = $_POST['name'];
        $this->password = $_POST['password'];
    }

    function createOutText() {
        $this->outText = $this->name.' '.$this->password.'  '.$this->getTime()."\r\n";
    }

    function getTime () {
        $str = strftime("%d.%m.%Y %H.%M.%S");
        return $str;
    }

    function saveUserData($filePath){
        $file = fopen((string)$filePath, 'a+');
        fputs($file, $this->outText);
        fclose($file);
    }

}