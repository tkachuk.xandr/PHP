<?php
include "../Task2/UserRecord2.php";
class UserRecord3 extends UserRecord2 {

    protected $db;
    protected $dbName = "queries";
    protected $tableName = "main_table";

    function __construct() {
        $this->createDB();
    }

    function createDB() {
        $this->db = new mysqli('127.0.0.1');
        if ($this->db->connect_errno) {
            echo "Connection error";
        }

        if(!$this->db->query("USE ".$this->dbName)) {
            echo "Use DB error";
            if(!$this->db->query("CREATE DATABASE ".$this->dbName)) {
                echo "Creation DB error";
            }
        }

        if(!$this->db->query("USE ".$this->tableName)) {
            echo "Use Table error";
            if(!$this->db->query("CREATE TABLE ".$this->dbName
                ."id TEXT, name TEXT, filename TEXT, time TEXT")) {
                echo "Creation Table error";
            }
        }


    }
}